angular.module('Amoozesh.Common')
    //.constant('CURRENT_BACKEND', 'node')
    // .constant('CURRENT_BACKEND', 'firebase')
    .constant('CURRENT_BACKEND', 'java')
    .service('EndpointConfigService', function($rootScope, CURRENT_BACKEND) {
        var service = this,
            endpointMap = {
                node: { URI: 'http://localhost:4000/', root: 'api/clients/', format: ''},
                django: { URI: 'http://localhost:8000/', root: 'showplace', format: ''},
                java: { URI: 'http://192.168.1.2:8080', root: '', format: ''}, // must set up last
            },
            currentEndpoint = endpointMap[CURRENT_BACKEND],
            userId = null,
            backend = CURRENT_BACKEND;

        service.getUrl = function(model) {
            // return currentEndpoint.URI + currentEndpoint.root + userId + model;
            return currentEndpoint.URI + currentEndpoint.root  + model;
        };

        service.getUrlForId = function(model, id) {
            return service.getUrl(model) + id + currentEndpoint.format;
        };

        service.getCurrentBackend = function() {
            return backend;
        };

        service.getCurrentFormat = function() {
            return currentEndpoint.format;
        };

        service.getCurrentURI = function() {
            return currentEndpoint.URI;
        };

        $rootScope.$on('onCurrentUserId', function(event, id){
            userId = id;
        });
    });
