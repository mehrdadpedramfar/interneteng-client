angular.module('Amoozesh.Common')
.service('FairModel',
function ($http,EndpointConfigService,UtilsService) {
    var service = this,
        MODEL = '/fairs/';

    service.all = function () {
        return $http.get(EndpointConfigService.getUrl(
            MODEL + EndpointConfigService.getCurrentFormat()))
                .then(
                    function(result) {
                        return UtilsService.objectToArray(result);
                    }
                );
    };

    service.fetch = function (fair_id) {
        return $http.get(
            EndpointConfigService.getUrlForId(MODEL, fair_id)
        );
    };

    service.create = function (fair) {
        return $http.post(EndpointConfigService.getUrl(
            MODEL + EndpointConfigService.getCurrentFormat()), fair
        );
    };


    service.update = function (fair_id, fair) {
        return $http.put(
            EndpointConfigService.getUrlForId(MODEL, fair_id), fair
        );
    };

    service.destroy = function (fair_id) {
        return $http.delete(
            EndpointConfigService.getUrlForId(MODEL, fair_id)
        );
    };

});