angular.module('Amoozesh.Common')
.service('UserModel',
function ($http,EndpointConfigService,UtilsService) {
    var service = this,
        MODEL = '/users/';



    service.login = function (user) {

        var url = EndpointConfigService.getUrl( MODEL ) +'login';
        return $http.post(url, user );
    };


    service.register = function (user,role) {

        var url = EndpointConfigService.getUrl( MODEL ) +'register/' +role;

        return $http.post(url, user );
    };


    service.edit = function (user) {

        var url = EndpointConfigService.getUrl( MODEL ) +'edit_profile';

        return $http.post(url, user );
    };


    service.teachers = function (token) {

        var url = EndpointConfigService.getUrl(MODEL ) + 'teacher/teacher';
        return $http.get(url,{headers : {'Authorization': token}})
                .then(
                    function(result) {
                        return UtilsService.objectToArray(result);
                    }
                );
    };



    service.all = function () {
        return $http.get(EndpointConfigService.getUrl(
            MODEL + EndpointConfigService.getCurrentFormat()))
                .then(
                    function(result) {
                        return UtilsService.objectToArray(result);
                    }
                );
    };

    service.fetch = function (member_id) {
        return $http.get(
            EndpointConfigService.getUrlForId(MODEL, member_id)
        );
    };

    service.create = function (member) {

        console.log(EndpointConfigService.getUrl( MODEL + EndpointConfigService.getCurrentFormat()));

        return $http.post(EndpointConfigService.getUrl(
            MODEL + EndpointConfigService.getCurrentFormat()), member
        );
    };


    service.update = function (member_id, member) {
        return $http.put(
            EndpointConfigService.getUrlForId(MODEL, member_id), member
        );
    };

    service.destroy = function (member_id) {
        return $http.delete(
            EndpointConfigService.getUrlForId(MODEL, member_id)
        );
    };

});