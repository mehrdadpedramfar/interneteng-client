angular.module('Amoozesh.Common')
.service('TermModel',
function ($http,EndpointConfigService,UtilsService) {
    var service = this,
        MODEL = '/terms/';


      service.all = function (token) {
          var url = EndpointConfigService.getUrl(MODEL ) + 'managerterms';
        return $http.get(url,{headers : {'Authorization': token}})
                .then(
                    function(result) {
                        return UtilsService.objectToArray(result);
                    }
                );
    };




    service.create = function (group , token) {

        var url = EndpointConfigService.getUrl(MODEL) + 'termregister';

        return $http.post(url, group ,{headers : {'Authorization': token}});
    };


    service.edit = function (course) {

        var url = EndpointConfigService.getUrl( MODEL ) +'edit_profile';

        return $http.post(url, course );
    };



    // service.all = function () {
    //     return $http.get(EndpointConfigService.getUrl(
    //         MODEL + EndpointConfigService.getCurrentFormat()))
    //             .then(
    //                 function(result) {
    //                     return UtilsService.objectToArray(result);
    //                 }
    //             );
    // };

    service.fetch = function (member_id) {
        return $http.get(
            EndpointConfigService.getUrlForId(MODEL, member_id)
        );
    };



    service.update = function (member_id, member) {
        return $http.put(
            EndpointConfigService.getUrlForId(MODEL, member_id), member
        );
    };

    service.destroy = function (member_id) {
        return $http.delete(
            EndpointConfigService.getUrlForId(MODEL, member_id)
        );
    };

});