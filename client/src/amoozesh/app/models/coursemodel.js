angular.module('Amoozesh.Common')
.service('CourseModel',
function ($http,EndpointConfigService,UtilsService) {
    var service = this,
        MODEL = '/courses/';


    service.create = function (course , token) {

        var url = EndpointConfigService.getUrl(MODEL) + 'courseregister';

        return $http.post(url, course ,{headers : {'Authorization': token}});
    };

    service.edit = function (course) {

        var url = EndpointConfigService.getUrl( MODEL ) +'edit_profile';

        return $http.post(url, course );
    };




      service.all = function (token) {
          var url = EndpointConfigService.getUrl(MODEL ) + 'managercourses';
        return $http.get(url,{headers : {'Authorization': token}})
                .then(
                    function(result) {
                        return UtilsService.objectToArray(result);
                    }
                );
    };




    service.fetch = function (member_id) {
        return $http.get(
            EndpointConfigService.getUrlForId(MODEL, member_id)
        );
    };



    service.update = function (member_id, member) {
        return $http.put(
            EndpointConfigService.getUrlForId(MODEL, member_id), member
        );
    };

    service.destroy = function (member_id) {
        return $http.delete(
            EndpointConfigService.getUrlForId(MODEL, member_id)
        );
    };

});