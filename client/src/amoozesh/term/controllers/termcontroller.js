angular.module('Amoozesh.Term')
.controller('TermCtrl',
// function (FAIR_ACTIVITY_FIELDS,$scope,UserModel, FairModel) {
function ($scope,TermModel,CourseModel,UserModel,$window,shareuser) {


     // alert('hi from ctrl');


    console.log('-------------');
    console.log($scope.desiredLocation = shareuser.get());
    console.log('-------------');


    var term = this;
    term.loggedInUser = $scope.desiredLocation = shareuser.get();
    term.loginUser ={};
    term.token = null;
    // user.detailsVisible = true;
    // user.currentStoryId = null;
    term.currentterm = null;
    term.editedterm = {};
    term.courses = [];
    // user.activity_fields = FAIR_ACTIVITY_FIELDS;
    //

    term.getTerms = function () {

 // alert('hi from get terms');
        TermModel.all(term.loggedInUser.token)
            .then(function (result) {
                // console.log(result);
                term.terms = result;
            })
    };

    term.getTerms();

    term.register = function () {
         term_json = JSON.stringify(term.editedterm);
        TermModel.create(term_json,term.loggedInUser.token)
            .then(function (result) {

                // user.token = Object.keys(result.data)[0];
                // course.registerd = Object.values(result.data)[0];

                $window.location.href = '/client/#/' + term.loggedInUser.role + '_term';

                // member.fairs = result;
            })
    };




    term.getCourses =function () {

     // alert('hi from get courses');

        CourseModel.all(term.loggedInUser.token)
            .then(function (result) {
                // console.log(result);
                term.allcourses = result;
            })
    };

    term.getCourses();


    term.getTeachers =function () {

     // alert('hi from get courses');

        UserModel.teachers(term.loggedInUser.token)
            .then(function (result) {
                // console.log(result);
                term.allteachers = result;
            })
    };

    term.getTeachers();




    term.add_courses = function () {
         term_json = JSON.stringify(term.editedterm);
        TermModel.create(term_json,term.loggedInUser.token)
            .then(function (result) {

                // user.token = Object.keys(result.data)[0];
                // course.registerd = Object.values(result.data)[0];

                $window.location.href = '/client/#/' + term.loggedInUser.role + '_term';

                // member.fairs = result;
            })
    };

    // course.add = function () {
    //     // alert(user.loginUser.lastName);
    //      course_json = JSON.stringify(course.editedCourse);
    //     // alert(user_json);
    //     CourseModel.create(course_json)
    //         .then(function (result) {
    //
    //             alert('success')
    //
    //             // user.token = Object.keys(result.data)[0];
    //             user.loggedInUser = Object.values(result.data)[0];
    //             user.loggedInUser.token = Object.keys(result.data)[0];
    //
    //
    //             console.log(user.loggedInUser.role);
    //             shareuser.set(user.loggedInUser);
    //
    //
    //
    //             $window.location.href = '/client/#/home_' + user.loggedInUser.role;
    //
    //             // member.fairs = result;
    //         })
    // };


    // user.register = function () {
    //     // alert(user.loginUser.lastName);
    //      user_json = JSON.stringify(user.loginUser);
    //     // alert(user_json);
    //     UserModel.register(user_json)
    //         .then(function (result) {
    //
    //             // user.token = Object.keys(result.data)[0];
    //             user.registerd = Object.values(result.data)[0];
    //
    //             $window.location.href = '/client/#/home';
    //
    //             // member.fairs = result;
    //         })
    // };
    //
    //
    // user.edit_profile = function () {
    //     // alert(user.loginUser.lastName);
    //      user_json = JSON.stringify(user.loginUser);
    //     // alert(user_json);
    //     UserModel.edit(user_json)
    //         .then(function (result) {
    //
    //             user.token = Object.keys(result.data)[0];
    //             user.cur_user = Object.values(result.data)[0];
    //
    //             $window.location.href = '/client/#/home';
    //
    //             // member.fairs = result;
    //         })
    // };


//     user.resetForm = function () {
//         member.currentMember = null;
//         member.editedMember = {};
//     };
//
//
//     user.getFairs =function () {
//         FairModel.all()
//             .then(function (result) {
//                 // console.log(result);
//                 member.fairs = result;
//             })
//     }
//
//
//     user.getMembers = function () {
//         MemberModel.all()
//             .then(function (result) {
//                 console.log(result.data);
//         });
//     };
//
//
//     user.createMember = function () {
//
//         // console.log(member.editedMember);
//     MemberModel.create(member.editedMember)
//         .then(function (result) {
//             member.resetForm();
//
//         });
// };
//
//     user.getFairs();
//     user.getMembers();

});

