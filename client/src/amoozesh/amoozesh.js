var myModule = angular.module('Amoozesh',
[
    'ngRoute',
    'ngAnimate',
    'ngMessages',
    'Amoozesh.Common',
    'Amoozesh.User',
    'Amoozesh.Course',
    'Amoozesh.Group',
    'Amoozesh.Term'
    ]);

myModule.config(function($routeProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'src/amoozesh/user/tmpl/login.html',
            controller: 'UserCtrl',
            controllerAs: 'user'
            })
        .when('/register', {
            templateUrl: 'src/amoozesh/user/tmpl/register.html',
            controller: 'UserCtrl',
            controllerAs: 'user'
            })
        .when('/home_student', {
            templateUrl: 'src/amoozesh/user/tmpl/home_student.html',
            controller: 'UserCtrl',
            controllerAs: 'user'
            })
        .when('/home_teacher', {
            templateUrl: 'src/amoozesh/user/tmpl/home_teacher.html',
            controller: 'UserCtrl',
            controllerAs: 'user'
            })
        .when('/home_manager', {
            templateUrl: 'src/amoozesh/user/tmpl/home_manager.html',
            controller: 'UserCtrl',
            controllerAs: 'user'
            })
        .when('/edit_course', {
            templateUrl: 'src/amoozesh/course/tmpl/edit_course.html',
            controller: 'CourseCtrl',
            controllerAs: 'course'
            })
        .when('/manager_courses', {
            templateUrl: 'src/amoozesh/course/tmpl/course_manager.html',
            controller: 'CourseCtrl',
            controllerAs: 'course'
            })
        .when('/edit_course', {
            templateUrl: 'src/amoozesh/course/tmpl/edit_course.html',
            controller: 'CourseCtrl',
            controllerAs: 'course'
            })
        .when('/manager_courses', {
            templateUrl: 'src/amoozesh/course/tmpl/course_manager.html',
            controller: 'CourseCtrl',
            controllerAs: 'course'
            })
        .when('/edit_group', {
            templateUrl: 'src/amoozesh/group/tmpl/edit_group.html',
            controller: 'GroupCtrl',
            controllerAs: 'group'
            })
        .when('/manager_groups', {
            templateUrl: 'src/amoozesh/group/tmpl/group_manager.html',
            controller: 'GroupCtrl',
            controllerAs: 'group'
            })
        .when('/edit_term', {
            templateUrl: 'src/amoozesh/term/tmpl/edit_term.html',
            controller: 'TermCtrl',
            controllerAs: 'term'
            })
        .when('/manager_term', {
            templateUrl: 'src/amoozesh/term/tmpl/term_manager.html',
            controller: 'TermCtrl',
            controllerAs: 'term'
            })
        .otherwise({redirectTo: '/login'});
        });

// myModule.value('FAIR_ACTIVITY_FIELDS', [
//     {name: 'Medical' , value: 'M'},
//     {name: 'Food' , value:'F'},
//     {name: 'Equipment' , value:'E'}
//
// ]);

myModule.factory('shareuser', function() {
 var savedData = {};
 function set(data) {
   savedData = data;
 }
 function get() {
  return savedData;
 }

 return {
  set: set,
  get: get
 }

});