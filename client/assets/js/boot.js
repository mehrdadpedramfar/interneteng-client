head.load(
    { file: 'vendor/lib/lib/jquery.min.js' },
    { file: 'vendor/lib/lib/angular.min.js' },
    { file: 'vendor/lib/lib/angular-messages.min.js' },
    { file: 'vendor/lib/lib/angular-route.min.js' },
    { file: 'vendor/lib/lib/angular-animate.min.js' },
    { file: 'vendor/lib/lib/bootstrap.min.js' },
    { file: 'vendor/lib/lib/TweenMax.min.js' },
    { file: 'vendor/lib/lib/firebase.js' },
    { file: 'vendor/lib/lib/angularfire.min.js' },
    { file: 'vendor/lib/lib/lock-7.min.js' },

    { file: 'src/amoozesh/app/app.js' },
    { file: 'src/amoozesh/app/controllers/maincontroller.js' },
    { file: 'src/amoozesh/app/models/usermodel.js' },
    { file: 'src/amoozesh/app/models/coursemodel.js' },
    { file: 'src/amoozesh/app/models/groupmodel.js' },
    { file: 'src/amoozesh/app/models/termmodel.js' },
    // { file: 'src/amoozesh/app/models/Fairsmodel.js' },
    { file: 'src/amoozesh/app/services/EndpointConfigService.js' },
    { file: 'src/amoozesh/app/services/UtilsService.js' },
    { file: 'src/amoozesh/user/user.js' },
    { file: 'src/amoozesh/user/controllers/usercontroller.js' },
    { file: 'src/amoozesh/course/course.js' },
    { file: 'src/amoozesh/course/controllers/coursecontroller.js' },
    { file: 'src/amoozesh/group/group.js' },
    { file: 'src/amoozesh/group/controllers/groupcontroller.js' },
    { file: 'src/amoozesh/term/term.js' },
    { file: 'src/amoozesh/term/controllers/termcontroller.js' },
    { file: 'src/amoozesh/amoozesh.js' },


    { file: 'src/amoozesh/css/index.css' },
    { file: 'src/amoozesh/css/override.css' },
    { file: 'src/amoozesh/js/add_course.js' },


    { file: 'vendor/foundation/css/app.css' },
    { file: 'vendor/foundation/css/foundation.min.css' },
    { file: 'vendor/foundation/js/foundation.min.js' },
    { file: 'vendor/foundation/js/what-input.js' }
);
